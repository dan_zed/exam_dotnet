﻿using log4net;
using Ninject;

namespace ExamPuntoticket.Config
{
    public static class Bootstrapper
    {
        public static IKernel Kernel;
        public static void Init()
        {
            LogFactory.Configure();
            Kernel = new StandardKernel();

            Kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger("Log")).InSingletonScope();
            Kernel.Bind<Class1>().To<Class1>().InTransientScope();
            Kernel.Bind<Class2>().To<Class2>().InTransientScope();
            Kernel.Bind<IServiceMail>().To<ServiceMail>().InTransientScope();

            JobsRegister.Register();
        }
    }
}