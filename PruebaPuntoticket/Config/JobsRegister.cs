﻿using System;
using Quartz;
using Quartz.Impl;

namespace ExamPuntoticket.Config
{
    public class JobsRegister
    {
        public static void Register()
        {
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();
            scheduler.JobFactory = new NInjectJobFactory(Bootstrapper.Kernel);

            var triggerEvery5Seconds = TriggerBuilder.Create()
                .WithIdentity("Every5Seconds", "ExamPuntoticket")
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(240).WithRepeatCount(1))
                .StartNow()
                .Build();

            var now = DateTime.Now.AddSeconds(20);
            var triggerOnce = TriggerBuilder.Create()
                .WithIdentity("Once", "ExamPuntoticket")
                .WithDailyTimeIntervalSchedule(x => x.StartingDailyAt(new TimeOfDay(now.Hour, now.Minute, now.Second)))
                .Build();

            var jobProcessFile = JobBuilder.Create(typeof(Class1))
                .WithIdentity("ProcessFileJob", "ExamPuntoticket")
                .WithDescription("Process file")
                .Build();
            scheduler.ScheduleJob(jobProcessFile, triggerEvery5Seconds);


            var jobSendMail = JobBuilder.Create(typeof(Class2))
              .WithIdentity("SendMailJob", "ExamPuntoticket")
              .WithDescription("Send mail")
              .Build();

            scheduler.ScheduleJob(jobSendMail, triggerOnce);

            scheduler.Start();
        }

        public static void StopScheduler()
        {
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();
            scheduler.Shutdown(true);
        } 
    }
}