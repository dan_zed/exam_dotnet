﻿using Ninject;
using Quartz;
using Quartz.Spi;

namespace ExamPuntoticket.Config
{
    public class NInjectJobFactory : IJobFactory
    {
        private readonly IKernel _kernel;

        public NInjectJobFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            IJobDetail jobDetail = bundle.JobDetail;
            System.Type jobType = jobDetail.JobType;
            return _kernel.Get(jobType) as IJob;
        }

        public void ReturnJob(IJob job)
        {
            _kernel.Release(job);
        }
    }
}