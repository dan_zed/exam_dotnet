﻿using System.Collections.Generic;
using System.IO;
using log4net;

namespace ExamPuntoticket
{
    public class ServiceMail : IServiceMail
    {
        private readonly ILog _log;

        public ServiceMail(ILog log)
        {
            _log = log;
        }

        public void SendMail(IEnumerable<FileInfo> files)
        {
            if (files == null) return;
            foreach (var fileInfo in files)
            {
                _log.Info("Send Mail File: " + fileInfo.Name);
            }
        }
    }
}