﻿using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using Quartz;
using System.Linq;

namespace ExamPuntoticket
{
    public class Class1 : IJob
    {
        private readonly ILog _log;

        public Class1(ILog log)
        {
            _log = log;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Start process file");
            string[] lines = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "code.txt"));

            _log.Debug("Read code types");
            var lista = lines
                    .Select(le => le.Split(','))
                    .Select(items => new { number = items[0], type = items[1] });

            _log.Debug("Save file numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "numerated_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var i in lista.Where(i => new String[] { "Vip", "Palco" }.Contains(i.type)))
                {
                    file.WriteLine(i.number);
                }
            }

            _log.Debug("Save file not numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "notnumerated_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var i in lista.Where(i => new String[] { "Cancha General", "Galería" }.Contains(i.type)))
                {
                    file.WriteLine(i.number);
                }
            }

            _log.Debug("Save file group for types");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "typegroup_" + DateTime.Now.Ticks + ".txt")))
            {
                int totalCanchaGeneral = lista.Where(i => i.type.Equals("Cancha General")).Count();
                int totalGaleria = lista.Where(i => i.type.Equals("Galería")).Count();
                int totalPalco = lista.Where(i => i.type.Equals("Palco")).Count();
                int totalVip = lista.Where(i => i.type.Equals("Vip")).Count();

                file.WriteLine(totalCanchaGeneral > 0 ? "[Cancha General, " + totalCanchaGeneral + "]" : null);
                file.WriteLine(totalPalco > 0 ? "[Palco, " + totalPalco + "]" : null);
                file.WriteLine(totalVip > 0 ? "[Vip, " + totalVip + "]" : null);
                file.WriteLine(totalGaleria > 0 ? "[Galería, " + totalGaleria + "]" : null);
            }            
        }
    }
}